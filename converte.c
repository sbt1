/* Daniel dos Santos Marques 071xxxx */
/* Pedro Grojsgold 0712051 */


#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


enum Code {utf8, utf32, latin1};


int decodeLatin1(int);
int decodeUtf8(int);
int decodeUtf32(int);
int encodeLatin1(int);
int encodeUtf8(int);
int encodeUtf32(int);


void error (const char *fmt, ...) {
	va_list argp;
	va_start(argp, fmt);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	exit(1);
}


enum Code getcode (const char *codename) {
	if (strcmp(codename, "latin1") == 0) return latin1;
	if (strcmp(codename, "utf8") == 0) return utf8;
	if (strcmp(codename, "utf32") == 0) return utf32;
	error("có́digo ’%s’ invá́lido: os có́digos aceitos são "
                   "’latin1’, ’utf8’ e ’utf32’\n", codename);
	return 0;
}


int main (int argc, char *argv[]) {
	FILE *in, *out;
	enum Code codein, codeout;
	int charCode, c[4];
	if (argc != 5)
		error("uso correto: %s code-in code-out file-in file-out\n", argv[0]);
	codein = getcode(argv[1]);
	codeout = getcode(argv[2]);
	in = fopen(argv[3], "rb");
	if (in == NULL)
		error("não foi possível abrir arquivo '%s'\n", argv[3]);
	out = fopen(argv[4], "wb");
	if (out == NULL)
		error("não foi possível abrir arquivo '%s'\n", argv[4]);

	while(!feof(in)) {
		switch(codein) {
			case latin1:
				c[0] = fgetc(in);
				charCode = decodeLatin1(c[0]);
				printf("c[0]:%c   c[0]:%d   cC:%d\n", c[0], c[0], charCode);
				break;
			case utf8:
				charCode = decodeUtf8(0);
				break;
			case utf32:
				charCode = decodeUtf32(0);
				break;
			default:
				break;
		}
		if(-1 == charCode) {
			continue;
		}
		switch(codeout) {
			case latin1:
				fputc(encodeLatin1(charCode), out);
				break;
			case utf8:
				break;
			case utf32:
				break;
			default:
				break;
		}
	}

	fclose(in);
	fclose(out);
	return 1;
}


int decodeLatin1(int code) {
	if(code >= 0 && code <= 127) {
		return code;
	} else {
		return -1;
	}
}


int decodeUtf8(int code) {
	if(code >= 0 && code <= 127) {
		return code;
	} else {
		return -1;
	}
}


int decodeUtf32(int code) {
	if(code >= 0 && code <= 127) {
		return code;
	} else {
		return -1;
	}
}


int encodeLatin1(int code) {
	if(code >= 0 && code <= 255/*TODO: Verificar se pode estar entre 127 e 255*/) {
		return code;
	} else {
		return -1;
	}
}


int encodeUtf8(int code) {
}


int encodeUtf32(int code) {
}
